package br.edu.unisep.covid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CovidReportApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CovidReportApiApplication.class, args);
	}

}
