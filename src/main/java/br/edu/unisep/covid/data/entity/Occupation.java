package br.edu.unisep.covid.data.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "occupation")
public class Occupation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_occupation")
    private Integer id;

    @Column(name = "name")
    private String name;

}
